package com.fcavalieri.knime.resetonload;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.knime.core.data.DataTableSpec;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.workflow.NodeContainer;
import org.knime.core.node.workflow.NodeContext;
import org.knime.core.node.workflow.NodeID;
import org.knime.core.node.workflow.WorkflowManager;

public class ResetOnLoadNodeModel extends NodeModel {
	private static final NodeLogger LOGGER = NodeLogger.getLogger(ResetOnLoadNodeModel.class);
	
	protected ResetOnLoadNodeModel() {
		//0 input tables and 0 output tables
		super(0, 0);		
	}

    @Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException {
		return new DataTableSpec[] {};
	}
    
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception {
		return new BufferedDataTable[] {};
	}

	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) {
	}

	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException {	
		selfReset();	    
	}

	private void selfReset() {
		NodeContext nodeContext = NodeContext.getContext();
	    new Thread(() -> {
	      LOGGER.warn("Resetting node");
	      try {
	        NodeContainer nodeContainer = nodeContext.getNodeContainer();
	        WorkflowManager workflowManager = (WorkflowManager) nodeContainer.getDirectNCParent();
	        Optional<NodeID> nodeId = workflowManager.findNodes(ResetOnLoadNodeModel.class, false)
	        			   .entrySet()
	        			   .stream()
	        			   .filter(entry -> entry.getValue() == this)
	        			   .map(entry -> entry.getKey())
	        			   .findFirst();
	        if (nodeId.isPresent()) {
	        	if (nodeContainer.getNodeContainerState().isExecuted()) {
                    workflowManager.resetAndConfigureNode(nodeId.get());                    
                }
	        } else {
	        	throw new IllegalStateException("Cannot find own node id");
	        }
	      } catch (Exception e) {
	        LOGGER.warn("Error while trying to reset the node", e);
	      } 
	    }).start();		
	}

	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException {	
	}

	@Override
	protected void loadInternals(File nodeInternDir, ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
	}

	@Override
	protected void saveInternals(File nodeInternDir, ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
	}

	@Override
	protected void reset() {
	}
}

