package com.fcavalieri.knime.resetonload;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

public class ResetOnLoadNodeFactory 
        extends NodeFactory<ResetOnLoadNodeModel> {

    @Override
    public ResetOnLoadNodeModel createNodeModel() {
        return new ResetOnLoadNodeModel();
    }

    @Override
    public int getNrNodeViews() {
        return 0;
    }

    @Override
    public NodeView<ResetOnLoadNodeModel> createNodeView(final int viewIndex,
            final ResetOnLoadNodeModel nodeModel) {
		return null;
    }

    @Override
    public boolean hasDialog() {
        return false;
    }

    @Override
    public NodeDialogPane createNodeDialogPane() {
		return null;
    }

}

