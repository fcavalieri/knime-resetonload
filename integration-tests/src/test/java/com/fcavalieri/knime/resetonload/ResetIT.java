package com.fcavalieri.knime.resetonload;

import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;

import com.fcavalieri.knime.testing.KnimeE2E;
import com.fcavalieri.knime.testing.KnimeE2EException;

public class ResetIT {

	private KnimeE2E knimeE2E = new KnimeE2E(false);

	@Test
	void resetWorkflow_4_0_1() throws IOException, InterruptedException, KnimeE2EException {
		knimeE2E.runWorkflow("4.0.1", 
				List.of(new KnimeE2E.Plugin("com.fcavalieri.knime.resetonload.plugin")),
				List.of(), 
				List.of(TestPaths.projectFolder().resolve("src/test/knime/resetTest4.0-4.2")));
	}

	@Test
	void resetWorkflow_4_1_4() throws IOException, InterruptedException, KnimeE2EException {
		knimeE2E.runWorkflow("4.1.4", 
				List.of(new KnimeE2E.Plugin("com.fcavalieri.knime.resetonload.plugin")),
				List.of(), 
				List.of(TestPaths.projectFolder().resolve("src/test/knime/resetTest4.0-4.2")));
	}

	@Test
	void resetWorkflow_4_2_5() throws IOException, InterruptedException, KnimeE2EException {
		knimeE2E.runWorkflow("4.2.5", 
				List.of(new KnimeE2E.Plugin("com.fcavalieri.knime.resetonload.plugin")),
				List.of(), 
				List.of(TestPaths.projectFolder().resolve("src/test/knime/resetTest4.0-4.2")));
	}

	@Test
	void resetWorkflow_4_3_4() throws IOException, InterruptedException, KnimeE2EException {
		knimeE2E.runWorkflow("4.3.4", 
				List.of(new KnimeE2E.Plugin("com.fcavalieri.knime.resetonload.plugin")),
				List.of(), 
				List.of(TestPaths.projectFolder().resolve("src/test/knime/resetTest4.3-4.6")));
	}

	@Test
	void resetWorkflow_4_4_4() throws IOException, InterruptedException, KnimeE2EException {
		knimeE2E.runWorkflow("4.4.4", 
				List.of(new KnimeE2E.Plugin("com.fcavalieri.knime.resetonload.plugin")),
				List.of(), 
				List.of(TestPaths.projectFolder().resolve("src/test/knime/resetTest4.3-4.6")));
	}

	@Test
	void resetWorkflow_4_5_2() throws IOException, InterruptedException, KnimeE2EException {
		knimeE2E.runWorkflow("4.5.2", 
				List.of(new KnimeE2E.Plugin("com.fcavalieri.knime.resetonload.plugin")),
				List.of(), 
				List.of(TestPaths.projectFolder().resolve("src/test/knime/resetTest4.3-4.6")));
	}

	@Test
	void resetWorkflow_4_6_1() throws IOException, InterruptedException, KnimeE2EException {
		knimeE2E.runWorkflow("4.6.1", 
				List.of(new KnimeE2E.Plugin("com.fcavalieri.knime.resetonload.plugin")),
				List.of(), 
				List.of(TestPaths.projectFolder().resolve("src/test/knime/resetTest4.3-4.6")));
	}
}
