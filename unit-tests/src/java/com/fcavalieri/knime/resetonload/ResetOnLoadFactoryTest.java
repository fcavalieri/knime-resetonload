package com.fcavalieri.knime.resetonload;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ResetOnLoadFactoryTest {
	
	@Test
    public void testGetNrNodeViews() {
		ResetOnLoadNodeFactory factory = new ResetOnLoadNodeFactory();
		
		int nrviews = factory.getNrNodeViews();
		
		assertEquals(0, nrviews);
	}
}